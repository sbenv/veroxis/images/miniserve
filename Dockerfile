FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.20.2

RUN apk add --no-cache "dumb-init" "rsync"

COPY "miniserve/miniserve" "/usr/local/bin/miniserve"

ENTRYPOINT ["/usr/bin/dumb-init"]
CMD ["/usr/local/bin/miniserve"]
