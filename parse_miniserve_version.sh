#!/bin/sh

sed '/svenstaro\/miniserve/!d' version_manifest.txt | tail -n 1 | cut -d' ' -f4 | cut -d'=' -f2 | cut -d'v' -f2
